import unittest
from unittest.mock import MagicMock

from catalog.catalog import Catalog
from catalog.db import DB
from catalog.person import Person
from catalog.peselservice import PeselService


class CatalogTest(unittest.TestCase):

    def test_get_person_by_pesel(self):
        # given
        p = Person("Jan", "Kowalski", "45093035582")
        mock = DB()  # stworzenie obiektu ktory bedzie mockiem, zero magii
        mock.get_person = MagicMock(return_value=p)  # szykujemy zachowanie jednej metody, to jest mock
        c = Catalog(mock, None)  # tworzymy testowany obiekt, spelniajac zaleznosc mockiem

        # when
        result = c.get_person("45093035582")  # wlasciwy test

        # then
        self.assertEqual(p, result)  # weryfikacja po tescie
        mock.get_person.assert_called_with("45093035582")  # dodatkowo weryfikujemy obiekt zastepczy

        # Mozna tez tak, zdefiniowac liste wolan i potem weryfikowac cala liste:
        # calls = [call("pesel")];
        # mock.get_person.assert_has_calls(calls, any_order=False)

    def test_get_first_pesel(self):
        # given
        p1 = Person("Jan", "Kowalski", "45093035582")
        p2 = Person("Jan", "Kowalski", "94120701066")
        mock = DB()
        mock.get_pesel_list = MagicMock(return_value=[p1, p2])
        c = Catalog(mock, None)

        # when
        result = c.get_pesel("Jan", "Kowalski")
        
        # then
        self.assertEqual(p1, result)
        mock.get_pesel_list.assert_called_with("Jan", "Kowalski")

    def test_get_pesel_list(self):
        # given
        p1 = Person("Jan", "Kowalski", "45093035582")
        p2 = Person("Jan", "Kowalski", "94120701066")
        mock = DB()
        mock.get_pesel_list = MagicMock(return_value=[p1, p2])
        c = Catalog(mock, None)

        # when
        result = c.get_pesel_list("Jan", "Kowalski")

        # then
        self.assertEqual([p1, p2], result)
        mock.get_pesel_list.assert_called_with("Jan", "Kowalski")

    def test_add_person_proper_pesel(self):
        # given
        mock = DB()
        mock.insert_person = MagicMock()
        mockPeselService = PeselService()
        mockPeselService.verify = MagicMock(return_value=True)
        c = Catalog(mock, mockPeselService)

        # when
        c.add_person("Jan", "Kowalski", "45093035582")

        # then
        mock.insert_person.called
        mockPeselService.verify("45093035582")

    def test_add_person_improper_pesel(self):
        # given
        mock = DB()
        mock.insert_person = MagicMock()
        mockPeselService = PeselService()
        mockPeselService.verify = MagicMock(return_value=False)
        c = Catalog(mock, mockPeselService)

        # when + then
        with self.assertRaises(Exception) as context: 
            c.add_person("Jan", "Kowalski", "01010101010")
        self.assertEqual("improper PESEL",str(context.exception))
